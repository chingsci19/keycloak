﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;

namespace KeyCloak.Controllers
{
    [Route("/auth")]
    public class APIController : Controller
    {
        private readonly IConfiguration configuration;

        public APIController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        [Route("get_signin")]
        [HttpGet]
        public async Task<IActionResult> GetSigninAsync()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(this.configuration["Oidc:Authority"]);
            var request = new HttpRequestMessage(HttpMethod.Post, "/auth/realms/STUDENT/protocol/openid-connect/token");

            var keyValues = new List<KeyValuePair<string, string>>();
            keyValues.Add(new KeyValuePair<string, string>("username", "chingtest"));
            keyValues.Add(new KeyValuePair<string, string>("password", "123456"));
            keyValues.Add(new KeyValuePair<string, string>("grant_type", "password"));
            keyValues.Add(new KeyValuePair<string, string>("client_id", this.configuration["Oidc:ClientId"]));
            keyValues.Add(new KeyValuePair<string, string>("client_secret", this.configuration["Oidc:ClientSecret"]));

            request.Content = new FormUrlEncodedContent(keyValues);
            var response = await client.SendAsync(request);


            //_client.PostAsync(this.configuration["Oidc:Authority"] + "/protocol/openid-connect/token", new StringContent(content, Encoding.Default, "application/x-www-form-urlencoded"));
            return new ObjectResult(JObject.Parse(response.Content.ReadAsStringAsync().Result));
        }
        [Route("signin")]
        [HttpPost]
        public async Task<IActionResult> SigninAsync([FromBody] Login login)
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(this.configuration["Oidc:Authority"]);
            var request = new HttpRequestMessage(HttpMethod.Post, "/auth/realms/STUDENT/protocol/openid-connect/token");
            var keyValues = new List<KeyValuePair<string, string>>();
            keyValues.Add(new KeyValuePair<string, string>("username", login.username));
            keyValues.Add(new KeyValuePair<string, string>("password", login.password));
            keyValues.Add(new KeyValuePair<string, string>("grant_type", "password"));
            keyValues.Add(new KeyValuePair<string, string>("client_id", this.configuration["Oidc:ClientId"]));
            keyValues.Add(new KeyValuePair<string, string>("client_secret", this.configuration["Oidc:ClientSecret"]));

            request.Content = new FormUrlEncodedContent(keyValues);
            var response = await client.SendAsync(request);


            //_client.PostAsync(this.configuration["Oidc:Authority"] + "/protocol/openid-connect/token", new StringContent(content, Encoding.Default, "application/x-www-form-urlencoded"));
            return new ObjectResult(JObject.Parse(response.Content.ReadAsStringAsync().Result));
        }
        [Route("refresh")]
        [HttpGet]
        public async Task<IActionResult> RefreshAsync(Token token)
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(this.configuration["Oidc:Authority"]);
            var request = new HttpRequestMessage(HttpMethod.Post, "/auth/realms/STUDENT/protocol/openid-connect/token");

            var keyValues = new List<KeyValuePair<string, string>>();
            keyValues.Add(new KeyValuePair<string, string>("username", token.username));
            keyValues.Add(new KeyValuePair<string, string>("refresh_token", token.token));
            keyValues.Add(new KeyValuePair<string, string>("grant_type", "refresh_token"));
            keyValues.Add(new KeyValuePair<string, string>("client_id", this.configuration["Oidc:ClientId"]));
            keyValues.Add(new KeyValuePair<string, string>("client_secret", this.configuration["Oidc:ClientSecret"]));

            request.Content = new FormUrlEncodedContent(keyValues);
            var response = await client.SendAsync(request);


            //_client.PostAsync(this.configuration["Oidc:Authority"] + "/protocol/openid-connect/token", new StringContent(content, Encoding.Default, "application/x-www-form-urlencoded"));
            return new ObjectResult(JObject.Parse(response.Content.ReadAsStringAsync().Result));
        }
        [Route("refresh")]
        [HttpPost]
        public async Task<IActionResult> RefreshPostAsync([FromBody] Token token)
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(this.configuration["Oidc:Authority"]);
            var request = new HttpRequestMessage(HttpMethod.Post, "/auth/realms/STUDENT/protocol/openid-connect/token");

            var keyValues = new List<KeyValuePair<string, string>>();
            keyValues.Add(new KeyValuePair<string, string>("username", token.username));
            keyValues.Add(new KeyValuePair<string, string>("refresh_token", token.token));
            keyValues.Add(new KeyValuePair<string, string>("grant_type", "refresh_token"));
            keyValues.Add(new KeyValuePair<string, string>("client_id", this.configuration["Oidc:ClientId"]));
            keyValues.Add(new KeyValuePair<string, string>("client_secret", this.configuration["Oidc:ClientSecret"]));

            request.Content = new FormUrlEncodedContent(keyValues);
            var response = await client.SendAsync(request);


            //_client.PostAsync(this.configuration["Oidc:Authority"] + "/protocol/openid-connect/token", new StringContent(content, Encoding.Default, "application/x-www-form-urlencoded"));
            return new ObjectResult(JObject.Parse(response.Content.ReadAsStringAsync().Result));
        }

        [Route("signout")]
        [HttpGet]
        public async Task<IActionResult> SignoutAsync(Token token)
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(this.configuration["Oidc:Authority"]);
            var request = new HttpRequestMessage(HttpMethod.Post, "/auth/realms/STUDENT/protocol/openid-connect/logout");

            var keyValues = new List<KeyValuePair<string, string>>();
            keyValues.Add(new KeyValuePair<string, string>("username", token.username));
            keyValues.Add(new KeyValuePair<string, string>("refresh_token", token.token));
            keyValues.Add(new KeyValuePair<string, string>("grant_type", "refresh_token"));
            keyValues.Add(new KeyValuePair<string, string>("client_id", this.configuration["Oidc:ClientId"]));
            keyValues.Add(new KeyValuePair<string, string>("client_secret", this.configuration["Oidc:ClientSecret"]));

            request.Content = new FormUrlEncodedContent(keyValues);
            var response = await client.SendAsync(request);


            //_client.PostAsync(this.configuration["Oidc:Authority"] + "/protocol/openid-connect/token", new StringContent(content, Encoding.Default, "application/x-www-form-urlencoded"));
            return new ObjectResult(response.Content.ReadAsStringAsync().Result);
        }
        [Route("signout")]
        [HttpPost]
        public async Task<IActionResult> SignoutPostAsync([FromBody] Token token)
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(this.configuration["Oidc:Authority"]);
            var request = new HttpRequestMessage(HttpMethod.Post, "/auth/realms/STUDENT/protocol/openid-connect/logout");

            var keyValues = new List<KeyValuePair<string, string>>();
            keyValues.Add(new KeyValuePair<string, string>("username", token.username));
            keyValues.Add(new KeyValuePair<string, string>("refresh_token", token.token));
            keyValues.Add(new KeyValuePair<string, string>("grant_type", "refresh_token"));
            keyValues.Add(new KeyValuePair<string, string>("client_id", this.configuration["Oidc:ClientId"]));
            keyValues.Add(new KeyValuePair<string, string>("client_secret", this.configuration["Oidc:ClientSecret"]));

            request.Content = new FormUrlEncodedContent(keyValues);
            var response = await client.SendAsync(request);


            //_client.PostAsync(this.configuration["Oidc:Authority"] + "/protocol/openid-connect/token", new StringContent(content, Encoding.Default, "application/x-www-form-urlencoded"));
            
            return new ObjectResult(response.Content.ReadAsStringAsync().Result);
        }

    }
}
